import { PersonaEdicionComponent } from './pages/persona/persona-edicion/persona-edicion.component';
import { ProductoComponent } from './pages/producto/producto.component';
import { PersonaComponent } from './pages/persona/persona.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
{path: 'persona', component: PersonaComponent, children: [
  // rutas para navegar
  // pacienteEdicionComponent: para insertar y actualizar
  { path: 'nuevo', component: PersonaEdicionComponent}, // el mismo componente pero la logica sera un poco distinta
  { path: 'edicion/:id', component: PersonaEdicionComponent}, // el dos puntos, parte dinamica para seleccionar uno en especifico
    ]},
{path: 'producto', component: ProductoComponent}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
