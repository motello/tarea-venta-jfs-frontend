import { Subject } from 'rxjs';
import { HOST } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Persona } from '../_model/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

    // crearemos una variable reactiva para poder manejar el caso de mandar refrescamiento al componente cuando se actualice un paciente
  // estoy diciendo: quiero una variable reactiva y dentro de el almacenamos un arreglo de pacintes
  // luego vamos a paciente edicion (componente hijo y llenar esa data alla)
  personaCambio = new Subject<Persona[]>();
  // mensaje de snackbar y usamos variable reactiva que seria la siguiente luego ir apaciente-edicion.component.ts e implementarlo alla con:
  // en this.pacienteService.mensajeCambio.next('Se modifico');

  mensajeCambio = new Subject<string>();

  url: string = `${HOST}/personas`;

  constructor(private http: HttpClient) { }

  listar() {
        return this.http.get<Persona[]>(this.url);
      }

  listarPersonaPorID(id: number) {
        return this.http.get<Persona>(`${this.url}/${id}`);
      }

  modificar(persona: Persona) {
        return this.http.put(this.url, persona);
      }

  eliminar(id: number) {
        return this.http.delete(`${this.url}/${id}`);
          }

          registrar(persona: Persona) {
            return this.http.post(this.url, persona); // paciente es el body lo que hacia en postman - lo transforma a json
          }
}
