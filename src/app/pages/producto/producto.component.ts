import { DialogoComponent } from './dialogo/dialogo.component';
import { ProductoService } from './../../_service/producto.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { Producto } from './../../_model/producto';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  displayedColumns = ['idProducto', 'nombre', 'marca', 'acciones'];
  datasource: MatTableDataSource<Producto>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private productoService: ProductoService, private dialog: MatDialog, private snackBar: MatSnackBar) { }

  ngOnInit() {
// para refrescar el datasource de productos otra vez

this.productoService.productosCambio.subscribe(data => {
  this.datasource = new MatTableDataSource(data);
  this.datasource.paginator = this.paginator;
  this.datasource.sort = this.sort;
});

this.productoService.mensaje.subscribe(data => {
  this.snackBar.open(data, 'Aviso', { duration: 2000 });
});


    this.productoService.listar().subscribe(data => {
      this.datasource = new MatTableDataSource(data);
      // referencias al paginador
      this.datasource.paginator = this.paginator;
      this.datasource.sort = this.sort;
    });

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.datasource.filter = filterValue;
  }

  openDialog(producto: Producto) {
   // console.log(producto);
   this.dialog.open(DialogoComponent, {
     width: '250px',
     disableClose: true,
     data: producto
   })
   
    }

    eliminar(producto: Producto) {
      this.productoService.eliminar(producto.idProducto).subscribe( data => {
        this.productoService.listar().subscribe(productos => {
          this.productoService.productosCambio.next(productos);
          this.productoService.mensaje.next('Se elimino');
        });
      });
    }

}
