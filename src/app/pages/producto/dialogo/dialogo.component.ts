import { Component, OnInit, Inject } from '@angular/core';
import { Producto } from '../../../_model/producto';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductoService } from '../../../_service/producto.service';

@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.component.html',
  styleUrls: ['./dialogo.component.css']
})
export class DialogoComponent implements OnInit {

  producto: Producto;

  // podemos recibir la informacion que nos mandan 
  constructor(private dialogRef: MatDialogRef<DialogoComponent>, @Inject(MAT_DIALOG_DATA) public data: Producto, 
  private productoService: ProductoService) { }


  ngOnInit() {
    this.producto = new Producto();
    this.producto.idProducto = this.data.idProducto;
    this.producto.nombre = this.data.nombre;
    this.producto.marca = this.data.marca;
  }

  cancelar() {
    this.dialogRef.close();
  }

  operar() {

    if (this.producto != null && this.producto.idProducto > 0) {
      this.productoService.modificar(this.producto).subscribe(data => {
        this.productoService.listar().subscribe(productos => {
          this.productoService.productosCambio.next(productos);
          this.productoService.mensaje.next('Se modifico');
        });
      });
    } else {
      this.productoService.registrar(this.producto).subscribe(data => {
          this.productoService.listar().subscribe(productos => {
            this.productoService.productosCambio.next(productos);
            this.productoService.mensaje.next('Se elimino');
          });
      });
    }
    this.dialogRef.close();
  }


}
