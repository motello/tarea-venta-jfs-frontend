import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { PersonaService } from './../../_service/persona.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Persona } from '../../_model/persona';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {


  

  dataSource: MatTableDataSource <Persona>;
  displayedColumns = [ 'idPersona', 'nombres', 'apellidos', 'acciones' ];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private personaService: PersonaService, private snackBar: MatSnackBar) { }

  ngOnInit() {

    //  PARA REFRESCAR = aplicamos la variable reactiva para volver a mostrar la data (esto que va dentro se copio del de abajo)
    this.personaService.personaCambio.subscribe(data => {// se llama cada vez que el subcompoente hace un cambio

      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.personaService.mensajeCambio.subscribe(data => {// obtengo la data que seria el mensaje
      // tengo que inyectar el snackbar para poderlo usar a continuacion con un mensaje emergente
  this.snackBar.open(data, 'Aviso', {duration: 2000});
    });

    this.personaService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data); // poblamos el datasource con la lis que viene del servicio y pasamos la data y luego 
      // ..en html definimos las columnas para mostrar el contenido de la data

      this.dataSource.paginator = this.paginator; // su paginador es lo que es amarrado del html en donde esta viewchild
      this.dataSource.sort = this.sort;
    });
 }

 applyFilter(filterValue: string) { // para filtrar nombres en la busqueda
  filterValue = filterValue.trim();
  filterValue = filterValue.toLowerCase();
  this.dataSource.filter = filterValue; // busco en el dataSource el metodo filter
}

eliminar(idPaciente: number) {
  this.personaService.eliminar(idPaciente).subscribe(data => {
    this.personaService.listar().subscribe(data => {
      this.personaService.personaCambio.next(data);
    this.personaService.mensajeCambio.next('Se elimino');
  });
  });
    }


}
