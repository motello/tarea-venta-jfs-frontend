import { PersonaService } from './../../../_service/persona.service';
import { Persona } from './../../../_model/persona';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-persona-edicion',
  templateUrl: './persona-edicion.component.html',
  styleUrls: ['./persona-edicion.component.css']
})
export class PersonaEdicionComponent implements OnInit {

  edicion = false; // para manejar lo del formulario en blanco 
  id: number;
  persona: Persona;
  // variable que funciona en el persona-edicion.component.html
  form: FormGroup; 


  constructor(private rout: ActivatedRoute, private router: Router, private personaService: PersonaService) {
    this.form = new FormGroup({
      // aqui crearemos la representacion del html pero en typescript y se crea un formulario en blanco apenas carga la pagina
      'id': new FormControl(0),
      'nombres': new FormControl(''),
      'apellidos': new FormControl(''),
    });

  }

  ngOnInit() {
    this.persona = new Persona();
    // de este route quiero extraer sus parametros
    // realmente lo que nos interesa es obtener el id para poder llenar automaticamente el formulario
    // entonces capturamos el id a continuacion con una programacion reactiva
    this.rout.params.subscribe((params: Params) => {
    this.id = params['id']; // la palabra id sale del path en routing
    this.edicion = params['id'] != null; // si viene el id es true sino viene el id la edicion es false
    this.initForm();

    }); // como ya se que es paramtero, digo a esos parametros
  }

  initForm() { // con esto iniciamos el formulario
    // la logica es: que cuando en la url venga /nuevo debe estar en blanco pero si viene algun valor el formulario
    // debene tener datos de base de datos
    if (this.edicion) { // si viene una url con /edicion o /numero etonces se pobla 
    // cargar la data del servicio en el formulario
    this.personaService.listarPersonaPorID(this.id).subscribe(data => { // le pasamos el id que viene en la url que es.... 
    // ... la de this.id = params['id']; esta arriba
      this.form = new FormGroup({
      'id': new FormControl(data.idPersona),
      'nombres': new FormControl(data.nombres),
      'apellidos': new FormControl(data.apellidos)
    });

    });
    }
      }

      operar() { // para trabajar el area de nuevo
        // primero necesito extraer la info que estoy capturando en el html
        // necesito mandar un objeto persona
        // la siguiente linea dice: este persona su id viene a ser lo que esta en el formlario del campo cuyo identificador es
        this.persona.idPersona = this.form.value['id'];
        this.persona.nombres = this.form.value['nombres'];
        this.persona.apellidos = this.form.value['apellidos'];
        if (this.edicion) {
        // actualizar
        this.personaService.modificar(this.persona).subscribe(data => {
        // ahora con lo que hice en persona.service.ts hare esto para actualizar
        this.personaService.listar().subscribe(personas => { // todos los personas que vienen de la base de datos
// utilizamos la variable reactiv que esta en service, en su funcion next podemos almacenar la informacion nueva que contiene el cambio
// ahor l component principl pacien deb recepcionar est cambi q sucedio n paciene edicion,esto se hara en el ngOninit de componente padre
        this.personaService.personaCambio.next(personas); 
// esta es la variable reactiva que viene de persona.service.ts y luego de aca nos vamos a persona.component.ts para reaccionar a cambios
        this.personaService.mensajeCambio.next('Se modifico'); 
        });
        });
        } else {
          // registrar
          this.personaService.registrar(this.persona).subscribe(data => {
            this.personaService.listar().subscribe(personas => { // todos los personas que vienen de la base de datos
// utilizamos la variable reactiv que esta en service, en su funcion next podemos almacenar la informacion nueva que contiene el cambio
// ahor l component principl pacien deb recepcionar est cambi q sucedio n paciene edicion,esto se hara en el ngOninit de componente padre
            this.personaService.personaCambio.next(personas); 
            this.personaService.mensajeCambio.next('Se registro');
          });
        });
          }
        // Cuando se cierre lo de nuevo o edicion que se vaya a el componente principal, por medio de router: Router
        // por medio de este router vamos a navegar hacia una definicion qeu este en el app.routing en este caso persona
        this.router.navigate(['persona']);
          }

}
